package com.sviatukhov;

public class Human {
    private String firstName;
    private String lastName;
    private String patronymic;

    public Human(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Human(String firstName, String lastName, String patronymic) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
    }

    public static void getFullName(Human human) {
        if (human.patronymic == null) {
            System.out.println("Полное имя (без отчества): " + human.firstName + " " + human.lastName);
        } else {
            System.out.println("Полное имя (с отчеством): " + human.firstName + " " + human.lastName + " "
                    + human.patronymic);
        }
    }

    public static void getShortName(Human human) {
        if (human.patronymic == null) {
            System.out.println("Фамилия и инициалы: " + human.lastName + " " + human.firstName.charAt(0) + ".");
        } else {
            System.out.println("Фамилия и инициалы: " + human.lastName + " " + human.firstName.charAt(0) + "."
                    + human.patronymic.charAt(0) + ".");
        }
    }


    @Override
    public String toString() {
        return "Human{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                '}';
    }
}
