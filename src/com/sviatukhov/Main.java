package com.sviatukhov;

/*
Задача 1 (разминка)

Написать класс Human, который содержит фамилию, имя и отчество человека

Реализовать 2 конструктора, первый принимает фамилию и имя, второй все 3 параметра
реализовать метод getFullName, который возвращает полное имя (Пупкин Иван Василиевич),
учесть что отчества может не быть (Пупкин Иван)
реализовать метод getShortName который возвращает фамилию и инициалы (Пупкин И. В. или Пупкин И., если отчества нет)
 */
public class Main {

    public static void main(String[] args) {
	Human human = new Human("Иван", "Пупкин", "Василиевич");
        System.out.println(human);
        Human.getFullName(human);
        Human.getShortName(human);
    }
}